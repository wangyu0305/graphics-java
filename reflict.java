package com.test;

import java.lang.reflect.Method;

public class reflict {
	
	private String getClassName() {
		return "com.test.reflectTestClass";
	}
	
	public void loadClass() {
		try {
			Class myClass = Class.forName(getClassName());
			//Use reflection to list methods and invoke them
			Method[] methods = myClass.getMethods();
			Object object = myClass.newInstance();
			for (int i = 0; i < methods.length; i++) {
				if (methods[i].getName().startsWith("say")) {
					System.out.println(" methods :"+methods[i].invoke(object));
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		new reflict().loadClass();
	}

}
