
import java.awt.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
//import java.awt.event;
import java.awt.Point;
import java.lang.Math;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class snowFlake extends JPanel{

    public snowFlake(){
        setPreferredSize(new Dimension(800, 800));
        setBackground(Color.WHITE);
    }

    public void paintComponent(Graphics pen){
        super.paintComponent(pen);
        drawSnowflake(400, 400, 5, pen);
    }

    public void drawKochEdge(int sx, int sy, int ex, int ey, int angle, Graphics pen){
        int length = (int)( Point.distance(sx, sy, ex, ey) /3.0);
        if (length < 2) {
            pen.drawLine(sx, sy, ex, ey);
        }
        else{
            int px = sx + (int) (length * Math.cos(Math.toRadians(angle)));
            int py = sy + (int) (length * Math.sin(Math.toRadians(angle + 180)));
            int qx = px + (int) (length * Math.cos(Math.toRadians(angle + 60)));
            int qy = py + (int) (length * Math.sin(Math.toRadians(angle + 60 + 180)));
            int rx = qx + (int) (length * Math.cos(Math.toRadians(angle - 60)));
            int ry = qy + (int) (length * Math.sin(Math.toRadians(angle - 60 + 180)));

            drawKochEdge(sx, sy, px, py, angle, pen);
            drawKochEdge(px, py, qx, qy, angle + 60, pen);
            drawKochEdge(qx, qy, rx, ry, angle - 60, pen);
            drawKochEdge(rx, ry, ex, ey, angle, pen);
        }
    }

    public void drawSnowflake(int cx, int cy, int size, Graphics pen){
        int px = cx + (int)(size * Math.cos(Math.toRadians(90)));
        int py = cy + (int)(size * Math.sin(Math.toRadians(90 + 180)));
        int qx = cx + (int)(size * Math.cos(Math.toRadians(-30)));
        int qy = cy + (int)(size * Math.sin(Math.toRadians(-30 + 180)));
        int rx = cx + (int)(size * Math.cos(Math.toRadians(210)));
        int ry = cy + (int)(size * Math.sin(Math.toRadians(210+180)));

//        pen.drawLine(px, py, qx, qy);
//        pen.drawLine(qx, qy, rx, ry);
//        pen.drawLine(rx, ry, px, py);
        drawKochEdge(px, py, qx, qy, -60, pen);
        drawKochEdge(qx, qy, rx, ry, 180, pen);
        drawKochEdge(rx, ry, px, py, 60, pen);
    }

    public static void main(String args[]){
        JFrame jFrame = new JFrame("snow flake");
        jFrame.add(new snowFlake());
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.pack();
        jFrame.setVisible(true);
    }
}