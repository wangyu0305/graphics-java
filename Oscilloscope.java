public class Oscilloscope {

    public static void main(String[] args) {
        stdDraw.setXscale(-1, +1);
        stdDraw.setYscale(-1, +1);

        double A    = Double.parseDouble(args[0]);    // amplitudes
        double B    = Double.parseDouble(args[1]);
        double wX   = Double.parseDouble(args[2]);    // angular frequencies
        double wY   = Double.parseDouble(args[3]);
        double phiX = Double.parseDouble(args[4]);    // phase factors
        double phiY = Double.parseDouble(args[5]);

        // convert from degrees to radians
        phiY = Math.toRadians(phiX);
        phiY = Math.toRadians(phiY);


        for (double t = 0.0; t < 10; t += 0.0001) {
            double x = A * Math.sin(wX * t + phiX);
            double y = B * Math.sin(wY * t + phiY);
            stdDraw.point(x, y);
            stdDraw.show(10);
        }
    }
   
}
